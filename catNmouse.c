///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Creel Patrocinio <creel@hawaii.edu>
/// @date    3/20/2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define DEFAULT_MAX_NUMBER 2048



int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n", argc );

   int maxValue = DEFAULT_MAX_NUMBER;
   int aGuess;
   int minValue = 1;
   int incorrectGuess = 1;
   srand(time(0));

   int theNumberImThinkingOf = (rand() % (maxValue - minValue + 1)) + minValue;


   do {
      printf( "OK cat, I'm thinking of a number from 1 to %d. Make a guess:\n \n", maxValue);
      scanf( "%d", &aGuess );

         if(aGuess < 1) {
            printf("Enter a number that's 1 or >1 \n");
         continue;

         }
         if(aGuess > maxValue) {
            printf("Enter a number that's less than 2048\n");
         continue;
   
         }
         if(aGuess > theNumberImThinkingOf) {
            printf("No cat, the number I'm thinking of is smaller than %d. \n", aGuess);
         continue;

         }
         if(aGuess < theNumberImThinkingOf) {
            printf("No cat, the number I'm thinking of is larger than %d.\n", aGuess);
         continue;
         }
         if(aGuess = theNumberImThinkingOf) {
            printf("YOU GOT ME!\n");

         incorrectGuess = 0;
         }

   }

   while(incorrectGuess);
   return 0;



}

